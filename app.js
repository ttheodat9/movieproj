const fs = require('fs');
const url = require('url');
const http = require('http');
const qs = require('querystring');
const mime = require('mime');
const mysql = require('mysql');
const port = 1000;
var newFile;

//Connecting to MySQL schema
const connMySQL = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root', 
    database: 'movie_system'
});
connMySQL.connect((error) => {
    if (error) console.log(error);
    console.log("Connected to MySQL!");
});

//Connecting to my server on port 1000
const server = http.createServer((req, res) => {
    let theUrl = url.parse(req.url, true).pathname;
    let theFile = `publicFiles${theUrl}`;

    if (theFile === 'publicFiles/')
        theFile = 'publicFiles/home.html';
    switch (theFile) {
        //Opening new forms
        case 'publicFiles/newcustomer':
            newFile = 0;
            addNew(req, res);
            break;
        case 'publicFiles/newmovie':
            newFile = 1;
            addNew(req, res);
            break;
        case 'publicFiles/rentform':
            newFile = 2;
            addNew(req, res);
            break;

        //Opening Lists
        case 'publicFiles/listsC':
            newFile = 0;
            loadlists(req, res);
            break;
        case 'publicFiles/listsM':
            newFile = 1;
            loadlists(req, res);
            break;
        case 'publicFiles/listsR':
            newFile = 2;
            loadlists(req, res);
            break;
        case 'publicFiles/listsAvailable':
            newFile = 3;
            loadlists(req, res);
            break;
        case 'publicFiles/listsRented':
            newFile = 4;
            loadlists(req, res);
            break;

        //Showing Search Results
        case 'publicFiles/searchC':
            newFile = 0;
            searchFor(req, res);
            break;
        case 'publicFiles/searchM':
            newFile = 1;
            searchFor(req, res);
            break;
        case 'publicFiles/searchR':
            newFile = 2;
            searchFor(req, res);
            break;

        //Editting Forms
        case 'publicFiles/editC':
            newFile = 0;
            editItems(req, res);
            break;
        case 'publicFiles/editM':
            newFile = 1;
            editItems(req, res);
            break;
        case 'publicFiles/editR':
            newFile = 2;
            editItems(req, res);
            break;
        case 'publicFiles/editAvailable':
            newFile = 1;
            editItems(req, res);
            break;
        case 'publicFiles/editRented':
            newFile = 1;
            editItems(req, res);
            break;

        //Deleting
        case 'publicFiles/delete':
            deleteItems(req, res);
            break;

        default:
            fs.readFile(theFile, (error, data) => {
                if (error) {
                    res.writeHead(404, { 'Content-Type': 'text/plain' });
                    return res.end('404: The resource was not found.');
                }
                res.writeHead(200, { 'Content-Type': mime.getType(theFile) });
                return res.end(data);
            });
            break;
    }
})
server.listen(port, () => {
    console.log(`Connected to Port:${port}!`);
});


//***All NEW Forms***//
let addNew = (req, res) => {
    let newForm = '';
    let insertCmd = '';
    req.on('data', (chunks) => {
        newForm += chunks;
    });
    req.on('end', () => {
        let newInfo = JSON.parse(newForm);
        if (newFile === 0) {
            insertCmd = 'INSERT INTO customers SET ?';
        } else if (newFile === 1) {
            insertCmd = 'INSERT INTO movies SET ?';
        } else if (newFile === 2) {
            insertCmd = 'INSERT INTO rents SET ?';
        }
        connMySQL.query(insertCmd, newInfo, (error, result) => {
            if (error) console.log(error);
        });
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end();
    });
}


//All Lists
let loadlists = (req, res) => {
    let searchInfo = '';
    req.on('data', (data) => {
        searchInfo += data;
    });
    req.on('end', () => {
        let formSearch = qs.parse(searchInfo);
        let listCmd = '';

        if (newFile === 0) {
            if (formSearch.query) {
                listCmd = "SELECT * FROM customers WHERE fullname LIKE '" + formSearch.query + "'";
            }
            else {
                listCmd = 'SELECT * FROM customers';
            }
        } else if (newFile === 1) {
            if (formSearch.query) {
                listCmd = "SELECT * FROM movies WHERE title LIKE '" + formSearch.query + "'";
            }
            else {
                listCmd = 'SELECT * FROM movies';
            }
        } else if (newFile === 2) {
            listCmd = 'SELECT rentID, rentDate, returnDate, price, fullname, title FROM rents r INNER JOIN customers c ON r.customerID = c.customerID INNER JOIN movies m ON r.movieID = m.movieID';
        }
        else if (newFile === 3) {
            listCmd = 'SELECT * FROM movies WHERE availability = "yes"';
        }
        else if (newFile === 4) {
            listCmd = 'SELECT * FROM movies WHERE availability = "no"';
        }
        connMySQL.query(listCmd, (error, result) => {
            if (error) {
                console.log('List Error');
                return;
            }
            res.end(JSON.stringify(result));
        });
    });
}

let searchFor = (req, res) => {
    let searchInfo = '';
    let searchCmd = '';
    req.on('data', (data) => {
        searchInfo += data;
    });
    req.on('end', function () {
        let formSearch = JSON.parse(searchInfo);
        switch (newFile) {
            case 0:
                searchCmd = `SELECT * FROM customers WHERE fullname LIKE '${formSearch[0]}' || fullname LIKE '${formSearch[0]}%' || fullname LIKE '%${formSearch[0]}%' || fullname LIKE '%${formSearch[0]}'`;
                break;
            case 1:
                searchCmd = `SELECT * FROM movies WHERE title LIKE '${formSearch[0]}' || title LIKE '${formSearch[0]}%' || title LIKE '%${formSearch[0]}%' || title LIKE '%${formSearch[0]}'`;
                break;
            case 2:
                searchCmd = `SELECT rentID, rentDate, returnDate, price, fullname, title FROM rents r INNER JOIN customers c ON r.customerID = c.customerID INNER JOIN movies m ON r.movieID = m.movieID WHERE rentDate LIKE '${formSearch[0]}'`;
                break;
            default:
                break;
        }
        connMySQL.query(searchCmd, (error, result) => {
            if (error) throw error;
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.end(JSON.stringify(result));
        })
    });
}

//Deleting Items
let deleteItems = (req, res) => {
    let ID;
    let delCmd = '';
    if (newFile === 0) {
        ID = url.parse(req.url, true).query.customerID;
        delCmd = `DELETE FROM customers WHERE customerID = ${ID}`;
    } else if (newFile === 1 || newFile === 3 || newFile === 4) {
        ID = url.parse(req.url, true).query.movieID;
        delCmd = `DELETE FROM movies WHERE movieID = ${ID}`;
    } else if (newFile === 2) {
        ID = url.parse(req.url, true).query.rentID;
        delCmd = `DELETE FROM rents WHERE rentID = ${ID}`;
    }
    console.log(newFile);
    connMySQL.query(delCmd, (error, result) => {
        if (error) throw error;
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end();
    });
}

//Editting Items
let editItems = (req, res) => {
    let newForm = '';
    let editCmd = '';
    req.on('data', (chunks) => {
        newForm += chunks;
    });
    req.on('end', () => {
        let updateInfo = JSON.parse(newForm);
        if (newFile === 0) {
            let cID = url.parse(req.url, true).query.customerID;
            editCmd = `UPDATE customers SET ? WHERE customerID = ${cID}`;
        } else if (newFile === 1) {
            let mID = url.parse(req.url, true).query.movieID;
            editCmd = `UPDATE movies SET ? WHERE movieID = ${mID}`;
        } else if (newFile === 2) {
            let rID = url.parse(req.url, true).query.rentID;
            editCmd = `UPDATE rents SET ? WHERE rentID = ${rID}`;
        }
        connMySQL.query(editCmd, [updateInfo], (error, result) => {
            if (error) throw error;
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.end();
        });
    });
}  