//Initialize the event SELECT2 once  the page is already loaded (DOM ready)
$(document).ready(function () {
    //This enables SELECT2 for selecting customers
    $("#searchName").select2({ 
        placeholder: 'Select customer...',
        ajax: {
            url: '/listsC',
            dataType: 'json',
            delay: 250,
            type: 'POST',
            data: function (params) {
                params.term = (typeof params.term === 'undefined') ? '' : params.term;
                return {
                    query: params.term + '%' // search term
                };
            },
            processResults: function (response, params) {
                return {
                    results: processCustomersResults(response) //The function processResults adds a new property to the response ("id")
                                                       //Which is expected by the result 
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, 
        templateResult: function (repo) {
            if (repo.loading) return repo.text;
            var markup = "<div class='select2-result-repository clearfix'>" + repo.fullname + "</div>";
            return markup;
        }, 
        templateSelection: function (repo) {
            if (repo.fullname) {
                $('#cID').val(repo.customerID);
                return repo.fullname;
            } else {
                return repo.text;
            }
        } 
    }); 

    //This enables SELECT2 for selecting movies
    $("#searchTitle").select2({
        placeholder: 'Select movie...',
        ajax: {
            url: '/listsM',
            dataType: 'json',
            delay: 250,
            type: 'POST',
            data: function (params) {
                params.term = (typeof params.term === 'undefined') ? '' : params.term;
                return {
                    query: params.term + '%' // search term
                };
            },
            processResults: function (response, params) {
                return {
                    results: processMoviesResults(response), //The function processResults adds a new property to the response ("id")
                    //Which is expected by the result 
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, 
        templateResult: function (repo) {
            if (repo.loading) return repo.text;
            var markup = "<div class='select2-result-repository clearfix'>" + repo.title + "</div>";
            return markup;
        }, //
        templateSelection: function (repo) {
            if (repo.title) {
                $('#movID').val(repo.movieID);
                return repo.title;
            } else {
                return repo.text;
            }
        } 
    });
});

function processCustomersResults(response) {
    for (var i in response) {
        response[i]['id'] = response[i].customerID;
    }
    return response;
}

function processMoviesResults(response) {
    for (var i in response) {
        response[i]['id'] = response[i].movieID;
    }
    return response;
}
