
///Most, if not all, functions have switch cases to differentiate between the data for customers, movies and rents.///

//All Submit Buttons on New Forms Switchcase Operations
switchNew = (caseNumber) => {
    switch (caseNumber) {
        case 0:
            //Creating variables to check the email, phone number and zip code inputs
            let zip = document.getElementById('zip').value.trim();
            let countZip = 0;
            let number = document.getElementById('phone').value.trim();
            let countNumber = 0;
            let emailAdd = document.getElementById('email').value.trim();
            let countEmail = 0;
            for (let index = 0; index < zip.length; index++) {
                if (!isNaN(zip[index])) {
                    countZip++;
                }
            }
            for (let index = 0; index < number.length; index++) {
                if (!isNaN(number[index])) {
                    countNumber++;
                }
            }
            for (let index = 0; index < emailAdd.length; index++) {
                if (emailAdd[index] === '@') {
                    countEmail++;
                }
            }

            //Checking for empty or false inputs
            if (document.getElementById('name').value.trim() === '' ||
                document.getElementById('home').value.trim() === '' ||
                number === '' ||
                emailAdd === '' ||
                zip === '') {
                swal('Empty!', 'Please fill out the entire form to submit it.\n Phone: ###-###-####\n Email: must have @\n Zip Code: #####', 'warning');
                return false;
            } else if (countEmail < 1 || countEmail > 1 ||
                countNumber < 10 || countNumber > 10 ||
                countZip < 5 || countZip > 5) {
                swal('Invalid Inputs!', 'Please fill out the entire form correctly.', 'error');
                return false;
            } else {
                let customer = {
                    fullname: $('#name').val(),
                    address: $('#home').val(),
                    phone: parseInt($('#phone').val()),
                    email: $('#email').val(),
                    zip: parseInt($('#zip').val()),
                }
                $.ajax({
                    url: '/newcustomer',
                    type: 'POST',
                    contentType: 'Application/JSON',
                    data: JSON.stringify(customer),
                    success: (data, status) => {
                        if (status === 'success') {
                            swal('Submitted!', `Welcome to our Movie System, ${customer.fullname} :)`, 'info');
                            $('#name').val('');
                            $('#home').val('');
                            $('#email').val('')
                            $('#phone').val('###-###-####');
                            $('#zip').val('#####');
                        }
                    }
                })
            }
            break;
        case 1:
            if (document.getElementById('title').value.trim() === '' ||
                document.getElementById('director').value.trim() === '' ||
                document.getElementById('actors').value.trim() === '') {
                swal('Empty!', 'Please fill out the entire form to submit it.', 'warning');
                return false;
            } else {
                let premiere = '';
                let available = '';

                if (document.getElementById("premiere").checked === true) {
                    premiere = 'yes';
                } else {
                    premiere = 'no';
                }
                if (document.getElementById("available").checked === true) {
                    available = 'yes';
                } else {
                    available = 'no';
                }

                let movie = {
                    title: $('#title').val(),
                    director: $('#director').val(),
                    actors: $('#actors').val(),
                    genre: $('#genre').val(),
                    premiereStatus: premiere,
                    availability: available
                }
                $.ajax({
                    url: '/newmovie',
                    type: 'POST',
                    contentType: 'Application/JSON',
                    data: JSON.stringify(movie),
                    success: (data, status) => {
                        if (status === 'success') {
                            swal('Submitted!', `The '${movie.title}' movie was successfully inserted.`, 'info');
                            $('#title').val('');
                            $('#director').val('');
                            $('#actors').val('');
                            document.getElementById("premiere").checked = false;
                            document.getElementById("available").checked = false;

                        }
                    }
                })
            }
            break;
        case 2:
            let priceFloat = document.getElementById('price').value.trim();
            if (document.getElementById('rentD').value.trim() === '' ||
                document.getElementById('searchName').value.trim() === '' ||
                document.getElementById('searchTitle').value.trim() === '' ||
                priceFloat === '') {
                swal('Empty!', 'Please fill out the entire form to submit it.', 'warning');
                return false;
            } else if (!parseFloat(priceFloat)) {
                swal('Invalid Input!', 'Please fill out the entire form correctly.', 'error');
                return false;
            } else {
                let rent = {
                    rentDate: $('#rentD').val(),
                    returnDate: $('#returnD').val(),
                    price: parseFloat($('#price').val()),
                    customerID: parseInt($('#cID').val()),
                    movieID: parseInt($('#movID').val()),
                }
                $.ajax({
                    url: '/rentform',
                    type: 'POST',
                    contentType: 'Application/JSON',
                    data: JSON.stringify(rent),
                    success: (data, status) => {
                        if (status === 'success') {
                            swal({
                                title: "Submitted!", text: "This rent is now stored in our records.", type:
                                    "success"
                            }).then(() => {
                                location.reload();
                            }
                            );
                        }
                    }
                })
            }
            break;
        default:
            break;
    }
};

//Displaying the lists
loadlists = (caseNumber) => {
    let url = '';
    let listID;
    switch (caseNumber) {
        case 0:
            url = '/listsC';
            break;
        case 1:
            url = '/listsM';
            break;
        case 2:
            url = '/listsR';
            break;
        case 3:
            url = '/listsAvailable';
            break;
        case 4:
            url = '/listsRented';
            break;
        default:
            break;
    }
    $.ajax({
        url: url,
        type: 'GET',
        success: (list, status) => {
            customList(caseNumber, JSON.parse(list));
        }
    })
};
let customList = (caseNumber, list) => {
    let content = '';
    switch (caseNumber) {
        //Customers
        case 0:
            list.forEach((item) => {
                content += `
                 <tr class="table-dark" style="color:purple">
                    <td>${item.fullname}</td>
                    <td>${item.address}</td>
                    <td>${item.phone}</td>
                    <td>${item.email}</td>
                    <td>${item.zip}</td>
        
                   <td>
                   <button type="submit" style="color:purple" title="edit customer" onclick=\'editCustom(${JSON.stringify(item)}, ${caseNumber})\'><i class="fa fa-edit" title="edit customer"></i></button>
                   <button type="submit" title="delete customer" onclick=\'delCustom(${item.customerID}, ${caseNumber})\'><i class="fa fa-trash" title="delete customer"></i></button>
                   </td>
                    </tr>`;
            })
            break;
        //Movies
        case 1:
            list.forEach((item) => {
                content += `
                     <tr class="table-dark" style="color:purple">
                     <td>${item.title}</td>
                     <td>${item.director}</td>
                     <td>${item.actors}</td>
                     <td>${item.genre}</td>
                     <td>${item.premiereStatus}</td>
                     <td>${item.availability}</td>
            
                       <td>
                       <button type="submit" style="color:purple" title="edit movie" onclick=\'editCustom(${JSON.stringify(item)}, ${caseNumber})\'><i class="fa fa-edit" title="edit movie"></i></button>
                       <button type="submit" title="delete movie" onclick=\'delCustom(${item.movieID}, ${caseNumber})\'><i class="fa fa-trash" title="delete movie"></i></button>
                       </td>
                        </tr>`;
            })
            break;
        //Rents
        case 2:
            list.forEach((item) => {
                content += `
                 <tr class="table-dark" style="color:purple">
                    <td>${item.rentID}</td>
                    <td>${new Date(item.rentDate)}</td>
                    <td>${new Date(item.returnDate)}</td>
                    <td>$${(item.price).toFixed(2)}</td>
                    <td>${item.fullname}</td>
                    <td>${item.title}</td>
        
                   <td>
                   <button type="submit" style="color:purple" title="edit rent" onclick=\'editCustom(${JSON.stringify(item)}, ${caseNumber})\'><i class="fa fa-edit" title="edit rent"></i></button>
                   <button type="submit" title="delete rent" onclick=\'delCustom(${item.rentID}, ${caseNumber})\'><i class="fa fa-trash" title="delete rent"></i></button>
                   </td>
                    </tr>`;
            })
            break;
        //Available Movies
        case 3:
            list.forEach((item) => {
                content += `
                         <tr class="table-dark" style="color:purple">
                         <td>${item.title}</td>
                         <td>${item.director}</td>
                         <td>${item.actors}</td>
                         <td>${item.genre}</td>
                         <td>${item.premiereStatus}</td>
                         <td>${item.availability}</td>
                
                           <td>
                           <button type="submit" style="color:purple" title="edit movie" onclick=\'editCustom(${JSON.stringify(item)}, ${caseNumber})\'><i class="fa fa-edit" title="edit movie"></i></button>
                           <button type="submit" title="delete movie" onclick=\'delCustom(${item.movieID}, ${caseNumber})\'><i class="fa fa-trash" title="delete movie"></i></button>
                           </td>
                            </tr>`;
            })
            break;
        //Rented Movies
        case 4:
            list.forEach((item) => {
                content += `
                             <tr class="table-dark" style="color:purple">
                             <td>${item.title}</td>
                             <td>${item.director}</td>
                             <td>${item.actors}</td>
                             <td>${item.genre}</td>
                             <td>${item.premiereStatus}</td>
                             <td>${item.availability}</td>
                    
                               <td>
                               <button type="submit" style="color:purple" title="edit movie" onclick=\'editCustom(${JSON.stringify(item)}, ${caseNumber})\'><i class="fa fa-edit" title="edit movie"></i></button>
                               <button type="submit" title="delete movie" onclick=\'delCustom(${item.movieID}, ${caseNumber})\'><i class="fa fa-trash" title="delete movie"></i></button>
                               </td>
                                </tr>`;
            })
            break;
    }
    $('#listtable').html(content);
}


//Searching Records
searchFor = (searchCase) => {
    let searchReq = '';
    switch (searchCase) {
        case 0:
            if (!$('#searchCustomers').val()) {
                swal('Empty!', 'Please type in a customer name to search.', 'warning');
                return false;
            } else {
                searchReq = [$("#searchCustomers").val()]
                $.ajax({
                    url: '/searchC',
                    type: 'POST',
                    data: JSON.stringify(searchReq),
                    success: (data, status) => {
                        if (status === 'success') {
                            customList(0, JSON.parse(data))
                        }
                    }
                })
            }
            break;
        case 1:
            if (!$('#searchMovies').val()) {
                swal('Empty!', 'Please type in a movie title to search.', 'warning');
                return false;
            } else {
                searchReq = [$("#searchMovies").val()]
                $.ajax({
                    url: '/searchM',
                    type: 'POST',
                    data: JSON.stringify(searchReq),
                    success: (data, status) => {
                        if (status === 'success') {
                            customList(1, JSON.parse(data))
                        }
                    }
                })
            }
            break;
        case 2:
            if (!$('#searchRent').val()) {
                swal('Empty!', 'Please type in a rent date to search.', 'warning');
                return false;
            } else {
                searchReq = [$("#searchRent").val()]
                $.ajax({
                    url: '/searchR',
                    type: 'POST',
                    data: JSON.stringify(searchReq),
                    success: (data, status) => {
                        if (status === 'success') {
                            customList(2, JSON.parse(data))
                        }
                    }
                })
            }
            break;
        default:
            break;
    }
};

//Deleting items
let delCustom = (item, caseNumber) => {
    switch (caseNumber) {
        case 0:
            swal({
                title: "Are you sure you want to delete this record?",
                text: "Once deleted, this record cannot be recovered.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("This record has been deleted.", {
                            icon: "success",
                        });

                        $.ajax({
                            url: `/delete?customerID=${item}`,
                            type: 'DELETE',
                            success: (data, status) => {
                                if (status === 'success') {
                                    loadlists(0);
                                }
                            }
                        })
                    } else { swal("Delete Process has been cancelled for this record!"); }
                });
            break;
        case 1:
            swal({
                title: "Are you sure you want to delete this record?",
                text: "Once deleted, this record cannot be recovered.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("This record has been deleted.", {
                            icon: "success",
                        });

                        $.ajax({
                            url: `/delete?movieID=${item}`,
                            type: 'DELETE',
                            success: (data, status) => {
                                if (status === 'success') {
                                    loadlists(1);
                                }
                            }
                        })
                    } else { swal("Delete Process has been cancelled for this record!"); }
                });
            break;
        case 2:
            swal({
                title: "Are you sure you want to delete this record?",
                text: "Once deleted, this record cannot be recovered.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("This record has been deleted.", {
                            icon: "success",
                        });

                        $.ajax({
                            url: `/delete?rentID=${item}`,
                            type: 'DELETE',
                            success: (data, status) => {
                                if (status === 'success') {
                                    loadlists(2);
                                }
                            }
                        })
                    } else { swal("Delete Process has been cancelled for this record!"); }
                });
            break;
        case 3:
            swal({
                title: "Are you sure you want to delete this record?",
                text: "Once deleted, this record cannot be recovered.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("This record has been deleted.", {
                            icon: "success",
                        });

                        $.ajax({
                            url: `/delete?movieID=${item}`,
                            type: 'DELETE',
                            success: (data, status) => {
                                if (status === 'success') {
                                    loadlists(3);
                                }
                            }
                        })
                    } else { swal("Delete Process has been cancelled for this record!"); }
                });
            break;
        case 4:
            swal({
                title: "Are you sure you want to delete this record?",
                text: "Once deleted, this record cannot be recovered.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("This record has been deleted.", {
                            icon: "success",
                        });

                        $.ajax({
                            url: `/delete?movieID=${item}`,
                            type: 'DELETE',
                            success: (data, status) => {
                                if (status === 'success') {
                                    loadlists(4);
                                }
                            }
                        })
                    } else { swal("Delete Process has been cancelled for this record!"); }
                });
            break;
    }
}

//Editting Items
let editCustom = (item, caseNumber) => {
    $('.modal').modal('show');

    switch (caseNumber) {
        //Customers
        case 0:
            $('#name').val(item.fullname);
            $('#home').val(item.address);
            $('#phone').val(item.phone);
            $('#email').val(item.email);
            $('#zip').val(item.zip);

            $('#edit').on('click', () => {
                //Creating variables to check the email, phone number and zip code inputs
                let zip = document.getElementById('zip').value.trim();
                let countZip = 0;
                let number = document.getElementById('phone').value.trim();
                let countNumber = 0;
                let emailAdd = document.getElementById('email').value.trim();
                let countEmail = 0;
                for (let index = 0; index < zip.length; index++) {
                    if (!isNaN(zip[index])) {
                        countZip++;
                    }
                }
                for (let index = 0; index < number.length; index++) {
                    if (!isNaN(number[index])) {
                        countNumber++;
                    }
                }
                for (let index = 0; index < emailAdd.length; index++) {
                    if (emailAdd[index] === '@') {
                        countEmail++;
                    }
                }

                //Checking for empty or false inputs
                if (document.getElementById('name').value.trim() === '' ||
                    document.getElementById('home').value.trim() === '' ||
                    number === '' ||
                    emailAdd === '' ||
                    zip === '') {
                    swal('Empty!', 'Please fill out the entire form to submit it.\n Phone: ###-###-####\n Email: must have @\n Zip Code: #####', 'warning');
                    return false;
                } else if (countEmail < 1 || countEmail > 1 ||
                    countNumber < 10 || countNumber > 10 ||
                    countZip < 5 || countZip > 5) {
                    swal('Invalid Inputs!', 'Please fill out the entire form correctly.', 'error');
                    return false;
                } else {
                    let customer = {
                        fullname: $('#name').val(),
                        address: $('#home').val(),
                        phone: parseInt($('#phone').val()),
                        email: $('#email').val(),
                        zip: parseInt($('#zip').val())
                    }
                    $.ajax({
                        url: `/editC?customerID=${item.customerID}`,
                        type: 'POST',
                        contentType: 'Application/JSON',
                        data: JSON.stringify(customer),
                        success: (data, status) => {
                            if (status === 'success') {
                                swal('Updated!');
                                $('.modal').modal('hide');
                                loadlists(0);
                            }
                        }
                    })
                }
            })
            break;
        //Movies
        case 1:
            $('#title').val(item.title);
            $('#director').val(item.director);
            $('#actors').val(item.actors);
            $('#genre').val(item.genre);
            if (item.premiereStatus === 'yes') {
                document.getElementById("premiere").checked = true;
            } else {
                document.getElementById("premiere").checked = false;
            }
            if (item.availability === 'yes') {
                document.getElementById("available").checked = true;
            } else {
                document.getElementById("available").checked = false;
            }

            $('#edit').on('click', () => {
                if (document.getElementById('title').value.trim() === '' ||
                    document.getElementById('director').value.trim() === '' ||
                    document.getElementById('actors').value.trim() === '') {
                    swal('Empty!', 'Please fill out the entire form to submit it.', 'warning');
                    return false;
                } else {
                    if (document.getElementById("premiere").checked === true) {
                        premiere = 'yes';
                    } else {
                        premiere = 'no';
                    }
                    if (document.getElementById("available").checked === true) {
                        available = 'yes';
                    } else {
                        available = 'no';
                    }

                    let movie = {
                        title: $('#title').val(),
                        director: $('#director').val(),
                        actors: $('#actors').val(),
                        genre: $('#genre').val(),
                        premiereStatus: premiere,
                        availability: available
                    }
                    $.ajax({
                        url: `/editM?movieID=${item.movieID}`,
                        type: 'POST',
                        contentType: 'Application/JSON',
                        data: JSON.stringify(movie),
                        success: (data, status) => {
                            if (status === 'success') {
                                swal('Updated!');
                                $('.modal').modal('hide');
                                loadlists(1);
                            }
                        }
                    })
                }
            })
            break;
        //Rents
        case 2:
            let rD1 = new Date(item.rentDate);
            let rD2 = new Date(item.returnDate);
            $('#rID').html(`<i class="fa fa-id-badge" aria-hidden="true"></i>Rent ID: ${item.rentID}`);
            $('#rentD').val(`${rD1.getFullYear()}-${("0" + (rD1.getMonth() + 1)).slice(-2)}-${("0" + rD1.getDate()).slice(-2)}`);
            $('#returnD').val(`${rD2.getFullYear()}-${("0" + (rD2.getMonth() + 1)).slice(-2)}-${("0" + rD2.getDate()).slice(-2)}`);
            $('#price').val((item.price).toFixed(2));

            $('#edit').on('click', () => {
                let priceFloat = document.getElementById('price').value.trim();
                if (document.getElementById('rentD').value.trim() === '' ||
                    document.getElementById('searchName').value.trim() === '' ||
                    document.getElementById('searchTitle').value.trim() === '' ||
                    priceFloat === '') {
                    swal('Empty!', 'Please fill out the entire form to submit it.', 'warning');
                    return false;
                } else if (!parseFloat(priceFloat)) {
                    swal('Invalid Input!', 'Please fill out the entire form correctly.', 'error');
                    return false;
                } else {
                    let rent = {
                        rentDate: $('#rentD').val(),
                        returnDate: $('#returnD').val(),
                        price: parseFloat($('#price').val()),
                        customerID: parseInt($('#cID').val()),
                        movieID: parseInt($('#movID').val()),
                    }

                    $.ajax({
                        url: `/editR?rentID=${item.rentID}`,
                        type: 'POST',
                        contentType: 'Application/JSON',
                        data: JSON.stringify(rent),
                        success: (data, status) => {
                            if (status === 'success') {
                                swal('Updated!');
                                $('.modal').modal('hide');
                                loadlists(2);
                            }
                        }
                    })
                }
            })

            break;
        //Available Movies
        case 3:
            $('#title').val(item.title);
            $('#director').val(item.director);
            $('#actors').val(item.actors);
            $('#genre').val(item.genre);
            if (item.premiereStatus === 'yes') {
                document.getElementById("premiere").checked = true;
            } else {
                document.getElementById("premiere").checked = false;
            }
            if (item.availability === 'yes') {
                document.getElementById("available").checked = true;
            } else {
                document.getElementById("available").checked = false;
            }

            $('#edit').on('click', () => {
                if (document.getElementById('title').value.trim() === '' ||
                    document.getElementById('director').value.trim() === '' ||
                    document.getElementById('actors').value.trim() === '') {
                    swal('Empty!', 'Please fill out the entire form to submit it.', 'warning');
                    return false;
                } else {
                    if (document.getElementById("premiere").checked === true) {
                        premiere = 'yes';
                    } else {
                        premiere = 'no';
                    }
                    if (document.getElementById("available").checked === true) {
                        available = 'yes';
                    } else {
                        available = 'no';
                    }

                    let movie = {
                        title: $('#title').val(),
                        director: $('#director').val(),
                        actors: $('#actors').val(),
                        genre: $('#genre').val(),
                        premiereStatus: premiere,
                        availability: available
                    }
                    $.ajax({
                        url: `/editAvailable?movieID=${item.movieID}`,
                        type: 'POST',
                        contentType: 'Application/JSON',
                        data: JSON.stringify(movie),
                        success: (data, status) => {
                            if (status === 'success') {
                                swal({
                                    title: "Update Success", text: "This record has been successfully updated :)", type:
                                        "success"
                                }).then(() => {
                                    location.reload();
                                }
                                );
                            }
                        }
                    })
                }
            })
        //Rented Movies
        case 4:
            $('#title').val(item.title);
            $('#director').val(item.director);
            $('#actors').val(item.actors);
            $('#genre').val(item.genre);
            if (item.premiereStatus === 'yes') {
                document.getElementById("premiere").checked = true;
            } else {
                document.getElementById("premiere").checked = false;
            }
            if (item.availability === 'yes') {
                document.getElementById("available").checked = true;
            } else {
                document.getElementById("available").checked = false;
            }

            $('#edit').on('click', () => {
                if (document.getElementById('title').value.trim() === '' ||
                    document.getElementById('director').value.trim() === '' ||
                    document.getElementById('actors').value.trim() === '') {
                    swal('Empty!', 'Please fill out the entire form to submit it.', 'warning');
                    return false;
                } else {
                    if (document.getElementById("premiere").checked === true) {
                        premiere = 'yes';
                    } else {
                        premiere = 'no';
                    }
                    if (document.getElementById("available").checked === true) {
                        available = 'yes';
                    } else {
                        available = 'no';
                    }

                    let movie = {
                        title: $('#title').val(),
                        director: $('#director').val(),
                        actors: $('#actors').val(),
                        genre: $('#genre').val(),
                        premiereStatus: premiere,
                        availability: available
                    }
                    $.ajax({
                        url: `/editRented?movieID=${item.movieID}`,
                        type: 'POST',
                        contentType: 'Application/JSON',
                        data: JSON.stringify(movie),
                        success: (data, status) => {
                            if (status === 'success') {
                                swal({
                                    title: "Update Success", text: "This record has been successfully updated :)", type:
                                        "success"
                                }).then(() => {
                                    location.reload();
                                }
                                );
                            }
                        }
                    })
                }
            })
        default:
            break;
    }
}
